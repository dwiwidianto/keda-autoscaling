#!/usr/bin/env bash

check_program() {
  if ! [ -x "$(command -v $1)" ]; then
    echo "$1 is not installed. Installation instructions are here $2" >&2

    if [ $3 ]; then
      echo
      echo "Exiting as $1 is required"
      echo
      exit 1
    fi
  fi
}

check_program terraform "https://www.terraform.io/downloads"
check_program helm "https://helm.sh/docs/intro/install/"
check_program k3d "https://k3d.io/v5.4.4/#installation" true
check_program kubectl "(if a must) https://kubernetes.io/docs/tasks/tools/" true

echo "Checking kubernetes cluster..."
echo

if ! kubectl cluster-info; then
  echo "Seems like there is no cluster running?"
  exit 1
fi

echo
echo "All checks passed"
